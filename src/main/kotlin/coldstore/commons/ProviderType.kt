package coldstore.commons

enum class ProviderType {
    AWS {
        override fun providerName() = "aws"
    };

    abstract fun providerName(): String
}