package coldstore

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default

class CArgs(parser: ArgParser) {
    val verbose by parser.flagging(
        "-v", "--verbose",
        help = "enable verbose mode"
    )

    val provider by parser.storing(
        "-P", "--provider",
        help = "name of the provider"
    ).default<String?>(null)

    val source by parser.storing(
        "-F", "--from",
        help = "source folder (optional)"
    ).default<String?>(null)


    val func by parser.positional(
        "FUNCTION",
        help = "function"
    ).default<String?>(null)
}