## coldstore

`kotlin` based project to push things to cold storage. Currently configured with AWS.

### Build

`gradle clean build`

### init a configiration

`java -jar coldstore.jar init -P aws` (provider AWS)

This creates a fils `aws.json` at `~/.coldstore`

```
{
  "userDir": "......",
  "vaultName": "coldstore_<accountId>_id",
  "accountId": ".......",
  "region": "..........",
  "accessKeyId": ".........",
  "secretAccessKey": "........."
}
```



### To copy to cold storage

`java -jar coldstore.jar copy -P aws` (provider AWS)

The files stored at `userDir` are archived to and purshed to AWS. The archived files are added to the folder `Coldstore_<accountId>_archived` which can be later deleted.


## Future work

- Add multi-part upload, progress monitoring
- extend to GCP
