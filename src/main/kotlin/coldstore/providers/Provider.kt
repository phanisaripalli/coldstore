package coldstore.providers

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.slf4j.LoggerFactory
import java.io.File


abstract class Provider() {
    abstract var name: String
    protected val logger = LoggerFactory.getLogger(this::class.java)!!

    protected val homeDir = System.getProperty("user.home") + File.separator + ".coldstore"

    protected fun checkHomeExists(): Boolean {
        val f = File(homeDir)
        val homeExists = f.exists() && f.isDirectory
        logger.info("homeexists? ${f.exists() && f.isDirectory}")

        return homeExists
    }

    protected fun createHome() {
        val success = File(homeDir).mkdirs()
        if (success) println("Directory $homeDir path was created successfully")
        else println("Failed to create directory path $homeDir")
    }

    private fun writeToFile(content: String, fileName: String) {
        val path = homeDir + File.separator + fileName
        File(path).writeText(content)

        logger.info("credentials written")
    }

    protected fun saveCredentials(credentials: Map<String, String>) {
        logger.info("saving credentials")
        val gsonPretty = GsonBuilder().setPrettyPrinting().create()

        val jsonCredentials: String = gsonPretty.toJson(credentials)
        writeToFile(jsonCredentials, "${name}.json")
    }

    abstract fun setup()
    abstract fun copy(source: String?)

}