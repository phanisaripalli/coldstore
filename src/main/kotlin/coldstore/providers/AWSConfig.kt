package coldstore.providers

class AWSConfig {
    var userDir: String? = null
    var vaultName: String? = null
    var region: String? = null
    val accountId: String? = null
    val accessKeyId: String? = null
    val secretAccessKey: String? = null
}