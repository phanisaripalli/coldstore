package coldstore.providers

import coldstore.commons.Commons
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.glacier.AmazonGlacier
import com.amazonaws.services.glacier.AmazonGlacierClient
import com.amazonaws.services.glacier.model.CreateVaultRequest
import com.amazonaws.services.glacier.model.DescribeVaultRequest
import com.amazonaws.services.glacier.transfer.ArchiveTransferManagerBuilder
import com.google.gson.Gson
import java.io.File
import java.io.FileReader
import java.util.*


class AWS() : Provider() {
    override var name: String = "aws" //coldstore.commons.Provider.AWS.providerName()

    init {
        logger.info("running provider ${name}")
    }

    override fun setup() {
        val homeExists = this.checkHomeExists()

        if (!homeExists) {
            logger.info("creating home dir")
            createHome()
        }

        val credentials = getCredentials()
        saveCredentials(credentials)

        createVault()
    }

    override fun copy(source: String?) {
        val awsConfig = getAWSConfig()
        val client = getAWSGlacierClient(awsConfig)

        val atmBuilder = ArchiveTransferManagerBuilder()
        atmBuilder.glacierClient = client

        val atm = atmBuilder.build()

        val archivedDir = "Coldstore_" + awsConfig.accountId + "_archived"

        File(awsConfig.userDir).list().forEach {
            if (it.endsWith(".zip")) {
                val archivedFilename = awsConfig.userDir + File.separator + it
                logger.info("Uploading $archivedFilename")
                val description = it + Date()

                var f = File(archivedFilename)

                atm.upload(awsConfig.vaultName, description, f)
                println(awsConfig.userDir + File.separator + archivedDir + File.separator + it)

                val archivedNewFilename = awsConfig.userDir + File.separator + archivedDir + File.separator + it
                f.renameTo(File(archivedNewFilename))
                logger.info("Moved to $archivedNewFilename")
            }
        }
    }

    private fun getCredentials(): Map<String, String> {
        println("enter the full path of the folder to transfer from")
        val userDir = readLine().toString()
        println(userDir)
        println(Commons.folderExists(userDir))

        println("enter account_id")
        val accountId = readLine().toString()

        println("enter region")
        val region = readLine().toString()

        println("enter access_key_id")
        val accessKeyId = readLine().toString()

        println("enter secret_access_key")
        val secretAccessKey = readLine().toString()

        val vaultName = "coldstore_${accountId}_${System.currentTimeMillis()}"

        return mapOf(
            "userDir" to userDir,
            "vaultName" to vaultName,
            "accountId" to accountId,
            "region" to region,
            "accessKeyId" to accessKeyId,
            "secretAccessKey" to secretAccessKey
        )
    }

    private fun getAWSConfig(): AWSConfig {
        val gson = Gson()
        val jsonFile = homeDir + File.separator + "aws.json"

        return gson.fromJson(FileReader(jsonFile), AWSConfig::class.java) as AWSConfig
    }

    private fun getAWSGlacierClient(awsCredentials: AWSConfig): AmazonGlacier {
        val awsCreds =
            AWSStaticCredentialsProvider(
                BasicAWSCredentials(
                    awsCredentials.accessKeyId,
                    awsCredentials.secretAccessKey
                )
            )

        return AmazonGlacierClient.builder()
            .withRegion(awsCredentials.region)
            .withCredentials(awsCreds)
            .build()
    }

    private fun createVault() {
        val awsCredentials = getAWSConfig()

        val client = getAWSGlacierClient(awsCredentials)

        val createVaultRequest = CreateVaultRequest()
            .withVaultName(awsCredentials.vaultName)

        client.createVault(createVaultRequest)

    }

    fun printVaultDetails() {
        val awsConfig = getAWSConfig()
        val client = getAWSGlacierClient(awsConfig)
        val request = DescribeVaultRequest()
            .withVaultName(awsConfig.vaultName)
        val result = client.describeVault(request)

        println(
            "\nCreationDate: " + result.creationDate +
                    "\nLastInventoryDate: " + result.lastInventoryDate +
                    "\nNumberOfArchives: " + result.numberOfArchives +
                    "\nSizeInBytes: " + result.sizeInBytes +
                    "\nVaultARN: " + result.vaultARN +
                    "\nVaultName: " + result.vaultName
        );
    }
}