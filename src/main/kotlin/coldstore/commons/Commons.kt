package coldstore.commons

import java.io.File

class Commons {
    companion object {
        fun folderExists(name: String): Boolean {
            val f = File(name)
            return f.exists() && f.isDirectory
        }
    }
}